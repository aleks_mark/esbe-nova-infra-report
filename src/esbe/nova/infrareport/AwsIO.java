package esbe.nova.infrareport;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import esbe.nova.infrareport.util.ClientIO;

public class AwsIO {

	public static Map<String, Map<String, String>> getEc2Instances() throws Exception {
		String stdout = ClientIO.aws("ec2", "describe-instances", "--query",
				"Reservations[*].Instances[*].[InstanceId,InstanceType,Tags,State]", "--output", "json");

		JsonElement root = new JsonParser().parse(stdout);

		Map<String, Map<String, String>> id2instance = new HashMap<>();

		for (JsonElement e1 : root.getAsJsonArray()) {
			for (JsonElement e2 : e1.getAsJsonArray()) {
				JsonArray arr = e2.getAsJsonArray();

				String instanceId = arr.get(0).getAsString();
				String instanceType = arr.get(1).getAsString();

				String instanceName = null;
				if (arr.get(2) != null && arr.get(2).isJsonArray()) {
					for (JsonElement e3 : arr.get(2).getAsJsonArray()) {
						JsonObject o3 = e3.getAsJsonObject();
						if (o3.get("Key").getAsString().equals("Name")) {
							instanceName = o3.get("Value").getAsString();
						}
					}
				}

				String instanceState = arr.get(3).getAsJsonObject().get("Name").getAsString();

				Map<String, String> map = new HashMap<>();
				map.put("InstanceType", instanceType);
				map.put("InstanceName", instanceName);
				map.put("InstanceState", instanceState);
				id2instance.put(instanceId, map);
				System.out.println("AWS-CLI: EC2 " + instanceId + " => " + map);
			}
		}

		return id2instance;
	}

	public static Map<String, String[]> getDbInstances() throws Exception {
		String stdout = ClientIO.aws("rds", "describe-db-instances", "--query",
				"DBInstances[].[DBInstanceIdentifier,DBInstanceClass,AllocatedStorage,Iops]", "--output", "text");

		Map<String, String[]> name2spec = new HashMap<>();

		for (String line : stdout.replace("\r\n", "\n").replace("\r", "\n").split("\n")) {
			if (line.trim().isEmpty()) {
				continue;
			}

			String[] parts = line.split("\t");

			String name = parts[0];
			String clazz = parts[1];
			String size = parts[2];
			String iops = parts[3];

			String[] parts3 = new String[] { clazz, size, iops };
			System.out.println("AWS-CLI: RDS " + name + "=" + Arrays.asList(parts3));
			name2spec.put(name, parts3);
		}

		return name2spec;
	}

}
