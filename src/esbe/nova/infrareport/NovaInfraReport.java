package esbe.nova.infrareport;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import esbe.nova.infrareport.util.ClientIO;
import esbe.nova.infrareport.util.Config;
import esbe.nova.infrareport.util.MainArgs;
import esbe.nova.infrareport.util.MainArgs.Input;
import esbe.nova.infrareport.util.TextUtil;

public class NovaInfraReport {
	/*
	 * this might be the worst code you've even seen, written in bit of a hurry - but it works, kinda... this is
	 * happy-go-lucky code with secret sauce: easy to completely rework when needed. don't try to refactor it into
	 * something nice, you have better things to do, trust me.
	 */

	private static String setInstanceConfig(String instanceId, String instanceType) throws Exception {
		return ClientIO.aws("rds", "modify-db-instance", //
				"--db-instance-identifier", instanceId, //
				"--db-instance-class", instanceType, //
				"--apply-immediately");

	}

	public static void main(String[] args) throws Exception {
		Input input = MainArgs.parse(args);
		Config.init(input.properties.get("config"));

		// long oneHour = 3600_000L;

		// while (true) {
		update();

		// Thread.sleep(oneHour);
		// }
	}

	private static void update() throws Exception {
		long t0 = System.currentTimeMillis();

		boolean doUploadToWiki = true;
		String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm"));

		Map<String, String> type2report = new ConcurrentHashMap<>();
		Map<String, Map<String, String>> env2comp2status = new ConcurrentHashMap<>();

		ExecutorService threadpool = Executors.newFixedThreadPool(10);

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {
				type2report.put("ec2", printEC2());
			}
		});

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {
				type2report.put("rds", printRDS());
			}
		});

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {
				type2report.put("health", printHealths());
			}
		});

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {
				type2report.put("deploys", printDeploys(env2comp2status));
			}
		});

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {
				type2report.put("restores", printRestores(env2comp2status));
			}
		});

		// this will slowly kick off further queries in printDeploys() and printRestores()
		JenkinsIO.asyncFillBuildStatusForAllEnvs(env2comp2status);

		threadpool.shutdown();
		threadpool.awaitTermination(60, TimeUnit.MINUTES);

		if (type2report.get("deploys") == null || type2report.get("health") == null || type2report.get("rds") == null
				|| type2report.get("ec2") == null || type2report.get("restores") == null) {
			System.out.println("Aborted upload due tot missing data.");
			return;
		}

		//

		StringBuilder confluenceMarkup = new StringBuilder();

		confluenceMarkup.append("\r\n\r\n");
		confluenceMarkup.append("\r\n\r\n");
		confluenceMarkup.append("h1. Nova Deployments / RDS specs (generated @ " + timestamp + ")");
		confluenceMarkup.append("\r\n\r\n");
		confluenceMarkup.append("\r\n\r\n");

		confluenceMarkup.append("h1. Deploys per aws-env/component\r\n");
		confluenceMarkup.append(type2report.get("deploys"));
		confluenceMarkup.append("\r\n\r\n");

		confluenceMarkup.append("h1. Healthchecks per aws-env/component\r\n");
		confluenceMarkup.append(type2report.get("health"));
		confluenceMarkup.append("\r\n\r\n");
		confluenceMarkup.append("-");
		confluenceMarkup.append("\r\n\r\n");
		confluenceMarkup.append("{include:PROD infra performance target}");
		confluenceMarkup.append("\r\n\r\n");

		confluenceMarkup.append("h1. RDS specs per aws-env/component\r\n");
		confluenceMarkup.append(type2report.get("rds"));
		confluenceMarkup.append("\r\n\r\n");

		confluenceMarkup.append("h1. EC2 specs per aws-env/component\r\n");
		confluenceMarkup.append(type2report.get("ec2"));
		confluenceMarkup.append("\r\n\r\n");

		confluenceMarkup.append("h1. Snapshot-restores per aws-env/component\r\n");
		confluenceMarkup.append(type2report.get("restores"));
		confluenceMarkup.append("\r\n\r\n");

		System.out.println(confluenceMarkup);

		if (doUploadToWiki) {
			ConfluenceIO.updateReportPage(confluenceMarkup.toString());
		}

		long t1 = System.currentTimeMillis();

		System.out.println("DONE [tagged " + timestamp + "] (took: " + (t1 - t0) / 1000 + "sec)");
	}

	public static String printHealths() throws Exception {
		Map<String, Map<String, Boolean[]>> comp2env2health = HealthcheckIO.checkHealthForAllComponentsForAllEnvs();

		StringBuilder sb = new StringBuilder();

		for (String env : Config.getGroupedEnvs()) {
			if (env == null) {
				sb.append("|| || ~ || ||");
				for (String component : Config.getNovaHealthComponents()) {
					sb.append(component + " ping||health|| ||");
				}
				sb.append("\r\n");

				continue;
			}

			sb.append("||");
			sb.append(env);
			sb.append("|");

			boolean isEntireEnvOK = true;
			for (String component : Config.getNovaHealthComponents()) {
				Map<String, Boolean[]> env2health = comp2env2health.get(component);
				if (env2health == null) {
					isEntireEnvOK = false;
				} else {
					Boolean[] health = env2health.get(env);
					for (Boolean state : health == null ? new Boolean[] { null } : health) {
						if (state == null || !state) {
							isEntireEnvOK = false;
						}
					}
				}
			}

			sb.append(isEntireEnvOK ? "(/)" : "(x)");
			sb.append("|| ");

			for (String component : Config.getNovaHealthComponents()) {
				sb.append("|");

				Map<String, Boolean[]> env2health = comp2env2health.get(component);
				if (env2health == null) {
					sb.append(" | ");
				} else {
					Boolean[] health = env2health.get(env);
					if (health == null) {
						sb.append(" | ");
					} else {
						for (Boolean state : health) {
							if (state == null) {
								sb.append("(?)|");
							} else if (state) {
								sb.append("(/)|");
							} else {
								sb.append("(x)|");
							}
						}
						sb.setLength(sb.length() - 1);
					}
				}

				sb.append("|| ");
			}

			sb.append("|");
			sb.append("\r\n");
		}

		return sb.toString();
	}

	public static String printEC2() throws Exception {

		Map<String, Map<String, String>> id2instance = AwsIO.getEc2Instances();

		StringBuilder sb = new StringBuilder();

		for (String env : Config.getGroupedEnvs()) {
			if (env == null) {
				sb.append("||");
				sb.append(" ");
				sb.append("||");
				for (String component : Config.getAwsEC2Components()) {
					sb.append(component);
					sb.append("||");
				}
				sb.append("\r\n");

				continue;
			}

			sb.append("||");
			sb.append(env);

			for (String component : Config.getAwsEC2Components()) {
				List<String> foundInstanceTypes = new ArrayList<>();
				for (Entry<String, Map<String, String>> entry : id2instance.entrySet()) {
					String instanceId = entry.getKey();
					Map<String, String> props = entry.getValue();
					String name = props.get("InstanceName");
					String instanceType = props.get("InstanceType");
					String instanceState = props.get("InstanceState");

					if (name == null) {
						continue;
					}
					if ("terminated".equalsIgnoreCase(instanceState)) {
						continue;
					}

					// odoo-eu-west-1a-1.dev10.nova.essent.be.
					if (!name.startsWith(component + "-") || !name.endsWith("." + env + ".nova.essent.be.")) {
						continue;
					}

					System.out.println("AWS-CLI: named instance env=" + env + ", comp=" + component + ", instance="
							+ instanceId + ", name=" + name);
					foundInstanceTypes.add(instanceType);
				}

				System.out.println(
						"AWS-CLI: instance-types env=" + env + ", comp=" + component + ", types=" + foundInstanceTypes);

				sb.append("|");
				if (foundInstanceTypes.isEmpty()) {
					sb.append("(!)");
				} else if (foundInstanceTypes.size() == 1) {
					sb.append(foundInstanceTypes.get(0));
				} else {
					Map<String, Integer> instanceType2freq = new HashMap<>();
					for (String instanceType : foundInstanceTypes) {
						Integer freq = instanceType2freq.get(instanceType);
						instanceType2freq.put(instanceType, (freq != null) ? freq + 1 : 1);
					}
					for (Entry<String, Integer> entry : instanceType2freq.entrySet()) {
						sb.append(entry.getKey() + " (" + entry.getValue() + "x)");
						sb.append("\r\n");
					}
					sb.setLength(sb.length() - 2);
				}
			}

			sb.append("|");
			sb.append("\r\n");
		}

		return sb.toString();
	}

	public static String printRDS() throws Exception {

		Map<String, String[]> name2spec = AwsIO.getDbInstances();

		StringBuilder sb = new StringBuilder();

		for (String env : Config.getGroupedEnvs()) {
			if (env == null) {
				sb.append("||");
				sb.append(" ");
				sb.append("||");
				for (String component : Config.getAwsRDSComponents()) {
					sb.append(component);
					sb.append("||");
					sb.append(" ||");
					sb.append(" ||");
					sb.append(" ||");
				}
				sb.append("\r\n");

				continue;
			}

			sb.append("||");
			sb.append(env);

			for (String component : Config.getAwsRDSComponents()) {
				String name = env + "-" + component + "-db-" + component;

				String[] spec = name2spec.get(name);
				if (spec == null) {
					spec = new String[3];
				}
				for (int i = 0; i < spec.length; i++) {
					if (spec[i] == null || spec[i].equals("None")) {
						spec[i] = " ";
					}
				}

				System.out.println(name);

				sb.append("|");
				sb.append(spec[0].startsWith("db.") ? spec[0].substring(3) : spec[0]);
				sb.append("|");
				sb.append(spec[1] + (spec[1].trim().isEmpty() ? "" : "G"));
				sb.append("|"); // easy on the zeroes... K?
				sb.append(spec[2].endsWith("000") ? spec[2].substring(0, spec[2].length() - 3) + "K" : spec[2]);
				sb.append("|| ");
			}
			sb.append("|");
			sb.append("\r\n");
		}

		return sb.toString();
	}

	public static interface Task {
		public void run() throws Exception;
	}

	public static void schedule(ExecutorService threadpool, Task task) {
		threadpool.execute(new Runnable() {
			@Override
			public void run() {
				try {
					task.run();
				} catch (Exception exc) {
					exc.printStackTrace();
				}
			}
		});
	}

	public static String printRestores(Map<String, Map<String, String>> env2comp2status) throws Exception {
		int workerCount = 8;
		Map<String, Map<String, Map<String, String>>> env2comp2buildParams = new ConcurrentHashMap<>();
		JenkinsIO.fetchDataRestoreInfo(workerCount, env2comp2status, env2comp2buildParams);

		StringBuilder sb = new StringBuilder();
		for (String env : Config.getGroupedEnvs()) {
			if (env == null) {
				sb.append("|| || || ||");
				for (String component : Config.getRestoreComponents()) {
					sb.append(component);
					sb.append("|| || ||");
				}
				sb.append("\r\n");

				continue;
			}

			sb.append("||");
			sb.append(env);

			Map<String, String> comp2snapshot = new HashMap<>();
			for (String component : Config.getRestoreComponents()) {
				String snapshot = env2comp2buildParams.get(env).get(component).get("DB_SNAPSHOT_NAME");
				if (snapshot != null) {
					snapshot = JenkinsIO.normalizeSnapshotName(snapshot, component);
				} else {
					snapshot = "(!)";
				}
				comp2snapshot.put(component, snapshot);
			}

			Set<String> snapshots = new HashSet<>(comp2snapshot.values());
			boolean areSnapshotsConsistent = (snapshots.size() == 1);
			boolean areRestoresOK = true;
			for (String component : Config.getRestoreComponents()) {
				Map<String, String> comp2status = env2comp2status.get(env);
				String job = JenkinsIO.getRestoreJob(env, component, comp2status);
				if (job == null) {
					job = "";
				}

				if (!"SUCCESS".equalsIgnoreCase(comp2status.get(job))) {
					areRestoresOK = false;
				}
			}

			sb.append("|");
			sb.append(mapBooleanConfluenceIcon(
					areRestoresOK ? (areSnapshotsConsistent ? Boolean.TRUE : null) : Boolean.FALSE));
			sb.append("|| ");

			Map<String, String> comp2status = env2comp2status.get(env);
			for (String component : Config.getRestoreComponents()) {
				String job = JenkinsIO.getRestoreJob(env, component, comp2status);
				if (job == null) {
					job = "";
				}
				String status = comp2status.get(job);
				status = (status == null) ? "" : status.toUpperCase();

				String snapshot = comp2snapshot.get(component);

				System.out
						.println("SNAPSHOT: " + env + "," + component + ", status=" + status + ", origin=" + snapshot);

				sb.append("|");
				sb.append(snapshot);
				sb.append("|");
				sb.append(mapJenkinsStatusToConfluenceIcon(status));
				sb.append("|| ");
			}
			sb.append("|");
			sb.append("\r\n");
		}

		return sb.toString();
	}

	public static String printDeploys(Map<String, Map<String, String>> env2comp2status) throws Exception {
		int workerCount = 8;
		Map<String, Map<String, Map<String, String>>> env2comp2buildParams = new ConcurrentHashMap<>();
		JenkinsIO.fetchServiceDeployInfo(workerCount, env2comp2status, env2comp2buildParams, 5 * 60_000);

		StringBuilder sb = new StringBuilder();
		for (String env : Config.getGroupedEnvs()) {
			if (env == null) {
				sb.append("||");
				sb.append(" ");
				sb.append("||");
				for (String component : Config.getDeployComponents()) {
					sb.append(component);
					sb.append("||");
					sb.append(" ||");
					sb.append(" ||");
					sb.append(" ||");
				}
				sb.append("\r\n");

				continue;
			}

			sb.append("||");
			sb.append(env);

			Map<String, String> comp2status = env2comp2status.get(env);
			for (String component : Config.getDeployComponents()) {
				String job = JenkinsIO.getDeployJob(env, component, comp2status);
				if (job == null) {
					job = "";
				}

				// this is highly dependent on levi9's perpetually shifting naming-conventions
				boolean hasFixedScaleOut = job.toLowerCase().contains("scaleout");
				boolean hasAutoScaleOut = JenkinsIO.lookupAutoScalingDeployJob(env, component, comp2status) != null;
				System.out.println("hasFixedScaleOut=" + hasFixedScaleOut);
				System.out.println("hasAutoScaleOut=" + hasAutoScaleOut);

				int masterCount = 0;
				int slaveCount = 0;
				if (hasAutoScaleOut) {
					// guess...
					masterCount = 1;
					slaveCount = 2;
				} else if (hasFixedScaleOut) {
					if (JenkinsIO.lookupDeployJob(env, component, "Master", comp2status) != null) {
						masterCount++;
					}
					for (int i = 1; i <= 10; i++) {
						if (JenkinsIO.lookupDeployJob(env, component, "Slave-" + i, comp2status) != null) {
							slaveCount++;
						}
					}
					if (masterCount == 0 && slaveCount == 0) {
						// no clue what this is about, but for a nice visual:
						masterCount = 1;
						slaveCount = 2;
					}
				} else {
					masterCount = 1;
					slaveCount = 0;
				}

				String status = comp2status.get(job);
				status = (status == null) ? "" : status.toUpperCase();

				Map<String, String> buildParams = env2comp2buildParams.get(env).get(component);

				String version = null;
				if (buildParams != null) {
					version = buildParams.get("VERSION");
					if (version == null) {
						// PREPROD/PROD jobs are special (in their own kind of way)
						version = buildParams.get(component.toUpperCase() + "_VERSION");
					}
					if (version == null && component.toUpperCase().equals("ENERGYCOMM")) {
						version = buildParams.get("MM_VERSION");
					}
				}

				if (version != null && version.endsWith("-SNAPSHOT")) {
					version = TextUtil.before(version, "-SNAPSHOT") + "-S";
				}
				if (version != null) {
					// version = version.replace("-", "&#8209;"); // don't chop me up

					// somebody copy/pasting this nifty value from the wiki would probably be pissed.
				}

				System.out.println(env + "/" + component + " ==> " + status + " ==> " + version);

				sb.append("|");

				// sb.append("[" + version + "](" + jenkinsURL + getLastBuildURL(env, component, comp2status) + ")");
				sb.append((version == null || version.trim().isEmpty()) ? "(!)" : version);

				sb.append("|");
				sb.append(mapJenkinsStatusToConfluenceIcon(status));
				sb.append("|");
				for (int i = 0; i < masterCount; i++) {
					sb.append("(*b)");
				}
				for (int i = 0; i < slaveCount; i++) {
					sb.append(hasAutoScaleOut ? "(*r)" : "(*y)");
				}
				if (masterCount + slaveCount == 0) {
					sb.append(" ");
				}
				sb.append("|| ");
			}
			sb.append("|");
			sb.append("\r\n");
		}

		return sb.toString();
	}

	private static String mapJenkinsStatusToConfluenceIcon(String status) {
		switch (status) {
		case "SUCCESS":
			return "(/)";
		case "FAILED":
			return "(x)";
		case "NOT BUILT":
			return "(off)";
		default:
			return "(?)";
		}
	}

	private static String mapBooleanConfluenceIcon(Boolean status) {
		if (status == null) {
			return "(!)";
		}
		if (status.equals(Boolean.TRUE)) {
			return "(/)";
		}
		return "(x)";
	}
}
