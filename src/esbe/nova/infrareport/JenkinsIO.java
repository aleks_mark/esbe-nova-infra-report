package esbe.nova.infrareport;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import esbe.nova.infrareport.NovaInfraReport.Task;
import esbe.nova.infrareport.util.ClientIO;
import esbe.nova.infrareport.util.Config;
import esbe.nova.infrareport.util.TextUtil;

public class JenkinsIO {

	public static void asyncFillBuildStatusForAllEnvs(Map<String, Map<String, String>> env2comp2status)
			throws Exception {
		// don't bring jenkins down
		int workerCount = 2;
		long staggeredCrawlDeploy = 2000L;

		ExecutorService threadpool = Executors.newFixedThreadPool(workerCount);

		for (String env : Config.getPackedEnvs()) {
			NovaInfraReport.schedule(threadpool, new Task() {
				@Override
				public void run() throws Exception {
					env2comp2status.put(env, JenkinsIO.getEnvStatuses(env));
					if (workerCount == 1) {
						Thread.sleep(staggeredCrawlDeploy);
					}
				}
			});

			if (workerCount > 1) {
				Thread.sleep(staggeredCrawlDeploy);
			}
		}

		threadpool.shutdown();
		threadpool.awaitTermination(60, TimeUnit.MINUTES);
	}

	public static void fetchDataRestoreInfo(int workerCount, //
			Map<String, Map<String, String>> env2comp2status, //
			Map<String, Map<String, Map<String, String>>> env2comp2buildParams //
	) throws Exception {
		ExecutorService threadpool = Executors.newFixedThreadPool(workerCount);

		for (String env : Config.getPackedEnvs()) {
			env2comp2buildParams.put(env, new ConcurrentHashMap<>());

			for (String component : Config.getRestoreComponents()) {
				env2comp2buildParams.get(env).put(component, new HashMap<>());
			}
		}

		CountDownLatch latch = new CountDownLatch(Config.getPackedEnvs().length * Config.getRestoreComponents().length);

		for (String env : Config.getPackedEnvs()) {
			NovaInfraReport.schedule(threadpool, new Task() {
				private int interval = 100;
				private int counter = 0;

				@Override
				public void run() throws Exception {
					Map<String, String> comp2status = env2comp2status.get(env);
					if (comp2status == null) {
						// data not available yet, retry later
						if (++counter % 50 == 0) {
							System.out.println("AWAITING DATA-RESTORE INFO FOR " + env + //
							" (for " + (counter * interval / 1000) + "sec)");
						}
						Thread.sleep(100);
						NovaInfraReport.schedule(threadpool, this);
						return;
					}

					for (Entry<String, String> entry : comp2status.entrySet()) {
						if (entry.getKey().toLowerCase().contains("restore")) {
							System.out.println("RESTORE STATUS: env=" + env + ", comp=" + entry.getKey() + " status="
									+ entry.getValue());
						}
					}

					for (String component : Config.getRestoreComponents()) {
						NovaInfraReport.schedule(threadpool, new Task() {
							@Override
							public void run() throws Exception {
								Map<String, String> buildParams = JenkinsIO.getRestoreLastBuildParameters(env,
										component, comp2status);
								System.out.println("RESTORE BUILD PARAMS: env=" + env + ", comp=" + component
										+ " params=" + buildParams);

								if (buildParams == null) {
									buildParams = new HashMap<>();
								}
								env2comp2buildParams.get(env).put(component, buildParams);

								latch.countDown();
							}
						});
					}
				}
			});
		}

		latch.await();
		threadpool.shutdown();
		threadpool.awaitTermination(60, TimeUnit.MINUTES);
	}

	public static void fetchServiceDeployInfo(int workerCount, //
			Map<String, Map<String, String>> env2comp2status, //
			Map<String, Map<String, Map<String, String>>> env2comp2buildParams, //
			long timeout
	) throws Exception {
		long staggeredCrawlDeploy = 2000L;
		ExecutorService threadpool = Executors.newFixedThreadPool(workerCount);

		for (String env : Config.getPackedEnvs()) {
			env2comp2buildParams.put(env, new ConcurrentHashMap<>());

			for (String component : Config.getDeployComponents()) {
				env2comp2buildParams.get(env).put(component, new HashMap<>());
			}
		}

		CountDownLatch latch = new CountDownLatch(Config.getPackedEnvs().length * Config.getDeployComponents().length);

		for (String env : Config.getPackedEnvs()) {
			NovaInfraReport.schedule(threadpool, new Task() {
				private int interval = 100;
				private int counter = 0;

				@Override
				public void run() throws Exception {
					Map<String, String> comp2status = env2comp2status.get(env);
					if (comp2status == null) {
						// data not available yet
						if (counter * interval > timeout) {
							// give up
							latch.countDown();
							return;
						}

						// retry later
						if (++counter % 50 == 0) {
							System.out.println("AWAITING SERVICE-DEPLOY INFO FOR " + env + //
							" (for " + (counter * interval / 1000) + "sec)");
						}
						Thread.sleep(interval);
						NovaInfraReport.schedule(threadpool, this);
						return;
					}

					for (Entry<String, String> entry : comp2status.entrySet()) {
						if (entry.getKey().toLowerCase().contains("deploy")) {
							System.out.println("DEPLOY STATUS: env=" + env + ", comp=" + entry.getKey() + " status="
									+ entry.getValue());
						}
					}

					for (String component : Config.getDeployComponents()) {
						NovaInfraReport.schedule(threadpool, new Task() {
							@Override
							public void run() throws Exception {
								Map<String, String> buildParams = JenkinsIO.getDeployLastBuildParameters(env, component,
										comp2status);
								System.out.println("DEPLOY BUILD PARAMS: env=" + env + ", comp=" + component
										+ " params=" + buildParams);

								if (buildParams == null) {
									buildParams = new HashMap<>();
								}
								env2comp2buildParams.get(env).put(component, buildParams);

								latch.countDown();
							}
						});
					}
				}
			});

			// don't bring jenkins down
			Thread.sleep(staggeredCrawlDeploy);
		}

		latch.await();
		threadpool.shutdown();
		threadpool.awaitTermination(60, TimeUnit.MINUTES);
	}

	public static String normalizeSnapshotName(String snapshot, String component) {
		snapshot = snapshot.toLowerCase();

		// we know it's an rds-snapshot
		if (snapshot.startsWith("rds:")) {
			snapshot = snapshot.substring(4);
		}

		// assume the correct snapshot for this database was used
		snapshot = snapshot.replace(component.toLowerCase(), "");

		// we know it's a database server
		snapshot = snapshot.replace("-db-", "-");

		// we don't care about read-replicas
		for (int i = 1; i < 10; i++) {
			snapshot = snapshot.replace("-rr" + i + "-", "-");
		}

		// we cleanup redundant dashes
		while (snapshot.contains("--")) {
			snapshot = snapshot.replace("--", "-");
		}

		return snapshot;
	}

	public static String getRestoreJob(String env, String component, Map<String, String> comp2status) {
		String job = "Restore-" + component + "-RDS-" + env;

		if (comp2status != null) {
			for (String comp : comp2status.keySet()) {
				if (job.equalsIgnoreCase(comp)) {
					return comp;
				}
			}
		}

		return null;
	}

	public static String lookupDeployJob(String env, String component, String suffix, Map<String, String> comp2status) {
		String job1 = "Deploy-" + component + "-" + env + (suffix == null ? "" : "-" + suffix);
		String job2 = "Deploy-" + component + (suffix == null ? "" : "-" + suffix) + "-" + env;

		if (comp2status != null) {
			for (String comp : comp2status.keySet()) {
				if (job1.equalsIgnoreCase(comp)) {
					return comp;
				}
				if (job2.equalsIgnoreCase(comp)) {
					return comp;
				}
			}
		}

		return null;
	}

	public static String lookupAutoScalingDeployJob(String env, String component, Map<String, String> comp2status) {
		String job3 = component + "-Update-AutoScale-" + env;

		if (comp2status != null) {
			for (String comp : comp2status.keySet()) {
				if (job3.equalsIgnoreCase(comp)) {
					return comp;
				}
			}
		}

		return null;
	}

	public static String getLastBuildURL(String env, String component, Map<String, String> comp2status)
			throws Exception {
		return getEnvViewURL(env) + "/job/" + JenkinsIO.getDeployJob(env, component, comp2status) + "/lastBuild";
	}

	public static String getLastBuildStatus(String env, String component, Map<String, String> comp2status)
			throws Exception {
		String xml = ClientIO.downloadJenkinsPage(getLastBuildURL(env, component, comp2status) + "/api/xml");
		if (xml == null) {
			return null;
		}

		return JenkinsIO.getBuildStatus(xml);
	}

	static Map<String, String> getDeployLastBuildParameters(String env, String component,
			Map<String, String> comp2status) throws Exception {
		String xml = ClientIO.downloadJenkinsPage(getEnvViewURL(env) + "/job/"
				+ JenkinsIO.getDeployJob(env, component, comp2status) + "/lastBuild/api/xml");
		if (xml == null) {
			return null;
		}

		try {
			return getBuildParametersXML(xml);
		} catch (Exception exc) {
			System.err.println(
					"IO-PARSE-ERROR: failed to parse build-params of Deploy-job for: " + env + ", " + component);
			return null;
		}
	}

	static Map<String, String> getRestoreLastBuildParameters(String env, String component,
			Map<String, String> comp2status) throws Exception {
		String xml = ClientIO.downloadJenkinsPage(getEnvViewURL(env) + "/job/"
				+ JenkinsIO.getRestoreJob(env, component, comp2status) + "/lastBuild/api/xml");
		if (xml == null) {
			return null;
		}

		try {
			return getBuildParametersXML(xml);
		} catch (Exception exc) {
			System.err.println(
					"IO-PARSE-ERROR: failed to parse build-params of Restore-job for: " + env + ", " + component);
			return null;
		}
	}

	private static String getEnvViewURL(String env) {
		if (Arrays.asList(Config.getNovaReportJenkinsAwsEnvsWithSillyViewPath().split(",")).contains(env)) {
			return "/view/" + env;
		}
		return "/view/Environments/view/" + env;
	}

	public static Map<String, String> getEnvStatuses(String env) throws Exception {
		System.out.println("JenkinsIO.getEnvStatuses(" + env + ")");
		String html = ClientIO.downloadJenkinsPage(getEnvViewURL(env) + "/");
		if (html == null) {
			return null;
		}

		String pattern = "<tr(.*?)id=\"(?<job>.*?)\"(.*?)tooltip=\"(?<status>.*?)\"";
		Map<String, String> parameters = new HashMap<>();
		for (Matcher m = Pattern.compile(pattern).matcher(html); m.find();) {
			String job = m.group("job").trim();
			if (job.startsWith("job_")) {
				job = job.substring(4);
			}
			String status = m.group("status").trim();
			parameters.put(job, status);

			System.out.println("JENKINS JOB STATUS: " + job + ": " + status);
		}
		return parameters;
	}

	public static String getDeployJob(String env, String component, Map<String, String> comp2status) {
		String job = lookupDeployJob(env, component, "ScaleOut", comp2status);
		if (job == null) {
			job = lookupDeployJob(env, component, null, comp2status);
		}
		return job;
	}

	static Map<String, String> getBuildParametersXML(String toSearch) {
		Map<String, String> parameters = new HashMap<>();

		TextUtil.between(toSearch, "<parameter", "</parameter>", new Function<String, Void>() {
			@Override
			public Void apply(String node) {
				String key = TextUtil.between(node, "<name>", "</name>").trim();
				String value = TextUtil.between(node, "<value>", "</value>").trim();

				parameters.put(key, value);
				return null;
			}
		});

		return parameters;
	}

	static String getBuildStatus(String xml) {
		return TextUtil.between(xml, "<result>", "</result>");
	}
}
