package esbe.nova.infrareport;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import esbe.nova.infrareport.util.ClientIO;
import esbe.nova.infrareport.util.Config;

public class ConfluenceIO {

	public static void updateReportPage(String confluenceMarkup) throws Exception {
		// String out =
		ConfluenceIO.updateConfluencePage(Config.getConfluenceWikiSpace(), Config.getConfluenceWikiPage(), //
				"Nova Deployments / RDS specs", confluenceMarkup.toString());
		// System.out.println(out);
	}

	public static String getConfluencePageVersion(String pageId) throws Exception {
		String response = ClientIO.requestConfluencePage("GET", "/rest/api/content/" + pageId, null);
		JsonObject responseObj = (JsonObject) new JsonParser().parse(response);
		return responseObj.getAsJsonObject("version").get("number").getAsString();
	}

	public static String updateConfluencePage(String space, String pageId, String title, String confluenceMarkup)
			throws Exception {

		String currentVersion = getConfluencePageVersion(pageId);
		System.out.println("CONFLUENCE PAGE REVISION: " + currentVersion);

		JsonObject payload = new JsonObject();
		payload.addProperty("type", "page");
		payload.addProperty("id", pageId);
		payload.addProperty("title", title);
		{
			JsonObject spaceObj = new JsonObject();
			spaceObj.addProperty("key", space);
			payload.add("space", spaceObj);
		}
		{
			JsonObject bodyObj = new JsonObject();
			{
				JsonObject storageObj = new JsonObject();
				storageObj.addProperty("value", confluenceMarkup);
				storageObj.addProperty("representation", "wiki");
				bodyObj.add("storage", storageObj);
			}
			payload.add("body", bodyObj);
		}
		{
			JsonObject versionObj = new JsonObject();
			versionObj.addProperty("number", Long.parseLong(currentVersion) + 1);
			versionObj.addProperty("status", "current");
			payload.add("version", versionObj);
		}

		return ClientIO.requestConfluencePage("PUT", "/rest/api/content/" + pageId, payload.toString());
	}
}
