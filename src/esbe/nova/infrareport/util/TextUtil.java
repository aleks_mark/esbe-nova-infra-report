package esbe.nova.infrareport.util;

import java.util.function.Function;

public class TextUtil {

	public static String before(String text, String find) {
		int io = text.indexOf(find);
		if (io == -1) {
			return null;
		}
		return text.substring(0, io);
	}

	public static String after(String text, String find) {
		int io = text.indexOf(find);
		if (io == -1) {
			return null;
		}
		return text.substring(io + find.length());
	}

	public static String afterLast(String text, String find) {
		int io = text.lastIndexOf(find);
		if (io == -1) {
			return null;
		}
		return text.substring(io + find.length());
	}

	public static String between(String text, String find1, String find2) {
		String after = after(text, find1);
		if (after == null) {
			return null;
		}
		return before(after, find2);
	}

	public static void between(String text, String find1, String find2, Function<String, Void> func) {
		while (true) {
			String after = after(text, find1);
			if (after == null) {
				return;
			}
			String between = before(after, find2);
			if (between == null) {
				return;
			}

			func.apply(between);

			text = after(after, find2);
		}
	}

	public static final String discardWhitespace(String s) {
		return s.replace(" ", "").replace("\t", "").replace("\r", "").replace("\n", "");
	}
}
