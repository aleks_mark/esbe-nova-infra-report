package esbe.nova.infrareport.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

public class Config {
	private static final Properties props = new Properties();

	public static void init(String path) {
		System.out.println("Loading config from: " + path);
		try (InputStream in = new FileInputStream(new File(path))) {
			props.load(in);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	private static String optionalProperty(String key) {
		return props.getProperty(key);
	}

	private static String requireProperty(String key) {
		String val = optionalProperty(key);
		if (val == null) {
			throw new NoSuchElementException("property not found: '" + key + "'");
		}
		return val;
	}

	private static String[] requirePropertyList(String key) {
		List<String> list = new ArrayList<>();
		for (String env : requireProperty(key).split(",")) {
			env = env.trim();
			if (env.isEmpty()) {
				continue;
			}
			if (env.equals("-")) {
				env = null;
			}
			list.add(env);
		}
		return list.toArray(new String[list.size()]);
	}

	public static String[] getGroupedEnvs() {
		return requirePropertyList("nova.report.layout");
	}

	private static String[] packed_envs;

	public static String[] getPackedEnvs() {
		if (packed_envs == null) {
			List<String> list = new ArrayList<>();
			for (String env : getGroupedEnvs()) {
				if (env != null) {
					list.add(env);
				}
			}
			packed_envs = list.toArray(new String[list.size()]);
		}

		return packed_envs;
	}

	public static String[] getDeployComponents() {
		return requirePropertyList("nova.report.deploy.components");
	}

	public static String[] getRestoreComponents() {
		return requirePropertyList("nova.report.restore.components");
	}

	public static String[] getAwsRDSComponents() {
		return requirePropertyList("nova.report.rds.components");
	}

	public static String[] getAwsEC2Components() {
		return requirePropertyList("nova.report.ec2.components");
	}

	public static String[] getNovaHealthComponents() {
		return requirePropertyList("nova.report.health.components");
	}

	public static String getConfluenceWikiSpace() {
		return requireProperty("confluence.spaceId");
	}

	public static String getConfluenceWikiPage() {
		return requireProperty("confluence.pageId");
	}

	public static String getPathToAwsCLI() {
		return requireProperty("aws.cli.path");
	}

	public static String[] getConfluenceCredentials() {
		return new String[] { requireProperty("confluence.credentials.user"),
				requireProperty("confluence.credentials.pass") };
	}

	public static String[] getJenkinsCredentials() {
		return new String[] { requireProperty("jenkins.credentials.user"),
				requireProperty("jenkins.credentials.pass") };
	}

	public static String[] getBPMCredentialsForHealthChecksWhichIsOddButOkay(String env) {
		return new String[] { requireProperty("health.bpm.credentials.user"),
				optionalProperty("health.bpm.credentials.pass." + env) };
	}

	public static String getConfluenceBaseURL() {
		return requireProperty("confluence.baseURL");
	}

	public static String getJenkinsBaseURL() {
		return requireProperty("jenkins.baseURL");
	}

	public static String getNovaEnvBaseURL() {
		return requireProperty("nova.env.baseURL");
	}

	public static String getNovaReportJenkinsAwsEnvsWithSillyViewPath() {
		return requireProperty("nova.report.jenkins.awsenvs.with.silly.view.path");
	}
}