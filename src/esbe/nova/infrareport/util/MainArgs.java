package esbe.nova.infrareport.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainArgs {
	public static class Input {
		public final Map<String, String> properties = new HashMap<>();
		public final Set<String> flags = new HashSet<>();
		public final List<String> tail = new ArrayList<>();
	}

	public static Input parse(String[] args) {
		Input input = new Input();
		for (int i = 0; i < args.length; i++) {
			String next = args[i].trim();
			if (next.startsWith("--")) {
				input.properties.put(next.substring(2), args[++i].trim());
			} else if (next.startsWith("-")) {
				next = next.trim();
				while (!next.isEmpty()) {
					input.flags.add(next.substring(1, 2));
					next = next.substring(1);
				}
			} else {
				for (; i < args.length; i++) {
					input.tail.add(args[i]);
				}
			}
		}
		return input;
	}
}
