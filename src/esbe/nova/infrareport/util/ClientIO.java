package esbe.nova.infrareport.util;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class ClientIO {
	public static String downloadJenkinsPage(String rel) throws Exception {
		String url = Config.getJenkinsBaseURL() + rel;

		URLConnection conn = new URL(url).openConnection();
		conn.setUseCaches(false);

		String[] creds = Config.getJenkinsCredentials();
		String encoded = Base64.getEncoder()
				.encodeToString((creds[0] + ":" + creds[1]).getBytes(StandardCharsets.UTF_8));
		conn.setRequestProperty("Authorization", "Basic " + encoded);

		InputStream in;
		try {
			in = conn.getInputStream();
		} catch (FileNotFoundException exc) {
			System.err.println("URL not found: "+url);
			return null;
		}

		return new String(readAll(in), StandardCharsets.UTF_8);
	}

	public static String downloadNovaPage(String env, String rel, String user, String pass) throws Exception {
		boolean isOdoo = false;
		if (rel.startsWith("/odoo/")) {
			rel = rel.substring(5);
			isOdoo = true;
		}
		String url = Config.getNovaEnvBaseURL().replace("${env}", isOdoo ? env + "-odoo" : env) + rel;

		URLConnection conn = new URL(url).openConnection();
		conn.setConnectTimeout(8 * 1000);
		conn.setReadTimeout(16 * 1000);
		conn.setUseCaches(false);

		if (user != null) {
			String encoded = Base64.getEncoder().encodeToString((user + ":" + pass).getBytes(StandardCharsets.UTF_8));
			conn.setRequestProperty("Authorization", "Basic " + encoded);
		}

		InputStream in;
		try {
			in = conn.getInputStream();
		} catch (IOException exc) {
			System.err.println("IO-Error: " + url + " " + exc.toString());
			return null;
		}

		return new String(readAll(in), StandardCharsets.UTF_8);
	}

	public static String requestConfluencePage(String method, String rel, String payload) throws Exception {
		String url = Config.getConfluenceBaseURL() + rel;

		System.out.println("CONFLUENCE URL: " + url);
		if (payload != null) {
			System.out.println("CONFLUENCE PAYLOAD: " + payload.toString());
		}

		URLConnection conn = new URL(url).openConnection();
		conn.setUseCaches(false);

		String[] creds = Config.getConfluenceCredentials();
		String encoded = Base64.getEncoder()
				.encodeToString((creds[0] + ":" + creds[1]).getBytes(StandardCharsets.UTF_8));
		conn.setRequestProperty("Authorization", "Basic " + encoded);

		conn.setDoOutput(true);
		((HttpURLConnection) conn).setRequestMethod(method);
		conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

		if (payload != null) {
			OutputStream out = conn.getOutputStream();
			out.write(payload.getBytes(StandardCharsets.UTF_8));
			out.flush();
		}

		InputStream in;
		try {
			in = conn.getInputStream();
		} catch (FileNotFoundException exc) {
			return null;
		}

		return new String(readAll(in), StandardCharsets.UTF_8);
	}

	public static String aws(String... args) throws Exception {
		String[] cmd = new String[args.length + 1];
		cmd[0] = Config.getPathToAwsCLI();
		System.arraycopy(args, 0, cmd, 1, args.length);

		Process proc = Runtime.getRuntime().exec(cmd);
		String stdout = new String(readAll(proc.getInputStream()), StandardCharsets.UTF_8);
		String stderr = new String(readAll(proc.getErrorStream()), StandardCharsets.UTF_8);
		int exitValue = proc.waitFor();

		if (exitValue != 0) {
			throw new IllegalStateException(stderr);
		}
		return stdout;
	}

	public static byte[] readAll(InputStream in) throws IOException {
		byte[] buf = new byte[4 * 1024];

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		while (true) {
			int got = in.read(buf, 0, buf.length);
			if (got == -1) {
				break;
			}
			baos.write(buf, 0, got);
		}

		return baos.toByteArray();
	}
}
