package esbe.nova.infrareport;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import esbe.nova.infrareport.util.ClientIO;
import esbe.nova.infrareport.util.Config;
import esbe.nova.infrareport.util.TextUtil;

public class HealthcheckIO {

	public static Map<String, Map<String, Boolean[]>> checkHealthForAllComponentsForAllEnvs() throws Exception {
		Map<String, Map<String, Boolean[]>> comp2env2health = new ConcurrentHashMap<>();

		ExecutorService threadpool = Executors.newFixedThreadPool(10);

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {
				comp2env2health.put("jbilling", checkComponentHealthForAllEnvs(//
						"jbilling", (env) -> {
							try {
								return checkJBilling(env);
							} catch (Exception e) {
								throw new IllegalStateException(e);
							}
						}));
			}
		});

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {
				comp2env2health.put("bpm", checkComponentHealthForAllEnvs(//
						"bpm", (env) -> {
							try {
								return checkBPM(env);
							} catch (Exception e) {
								throw new IllegalStateException(e);
							}
						}));
			}
		});

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {

				comp2env2health.put("odoo", checkComponentHealthForAllEnvs(//
						"odoo", (env) -> {
							try {
								return checkOdoo(env);
							} catch (Exception e) {
								throw new IllegalStateException(e);
							}
						}));
			}
		});

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {

				comp2env2health.put("crm", checkComponentHealthForAllEnvs(//
						"crm", (env) -> {
							try {
								return checkCRM(env);
							} catch (Exception e) {
								throw new IllegalStateException(e);
							}
						}));
			}
		});

		NovaInfraReport.schedule(threadpool, new NovaInfraReport.Task() {
			@Override
			public void run() throws Exception {

				comp2env2health.put("servicemix", checkComponentHealthForAllEnvs(//
						"servicemix", (env) -> {
							try {
								return checkServicemix(env);
							} catch (Exception e) {
								throw new IllegalStateException(e);
							}
						}));
			}
		});

		threadpool.shutdown();
		threadpool.awaitTermination(60, TimeUnit.MINUTES);

		return comp2env2health;
	}

	public static Map<String, Boolean[]> checkComponentHealthForAllEnvs(String comp,
			Function<String, Boolean[]> callback) throws Exception {
		ConcurrentHashMap<String, Boolean[]> env2health = new ConcurrentHashMap<>();

		CountDownLatch countDown = new CountDownLatch(1 + Config.getPackedEnvs().length);

		for (String env : Config.getPackedEnvs()) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Boolean[] result = callback.apply(env);
						System.out.println(
								"HEALTH CHECK: env=" + env + ", comp=" + comp + " => " + Arrays.toString(result));
						env2health.put(env, result);
					} catch (Exception exc) {
						exc.printStackTrace();
					} finally {
						countDown.countDown();
					}
				}
			}).start();
		}

		countDown.countDown();
		countDown.await();

		return env2health;
	}

	public static final Boolean[] checkServicemix(String env) throws Exception {
		String servicemixPing = "/servicemix/check/ping";
		String servicemixHealth = "/servicemix/check/health";

		String pingResp = ClientIO.downloadNovaPage(env, servicemixPing, null, null);
		String healthResp = ClientIO.downloadNovaPage(env, servicemixHealth, null, null);

		Boolean pingOK = (pingResp == null) ? null //
				: TextUtil.discardWhitespace(pingResp).contains("OK");
		Boolean healthOK = (healthResp == null) ? null //
				: TextUtil.discardWhitespace(healthResp).contains("{\"result\":true,\"msg\"");

		return new Boolean[] { pingOK, healthOK };
	}

	public static final Boolean[] checkCRM(String env) throws Exception {
		String crmPing = "/nova-crm/Api/V8_Custom/check/ping";
		String crmHealth = "/nova-crm/Api/V8_Custom/check/health";

		String pingResp = ClientIO.downloadNovaPage(env, crmPing, null, null);
		String healthResp = ClientIO.downloadNovaPage(env, crmHealth, null, null);

		Boolean pingOK = (pingResp == null) ? null //
				: TextUtil.discardWhitespace(pingResp).contains("<rs-response><result>true</result></rs-response>");
		Boolean healthOK = (healthResp == null) ? null //
				: TextUtil.discardWhitespace(healthResp).contains("<result>true</result><msg>OK</msg></checkResponse>");

		return new Boolean[] { pingOK, healthOK };
	}

	public static final Boolean[] checkOdoo(String env) throws Exception {
		String odooPing = "/odoo/check/ping";
		String odooHealth = "/odoo/check/health";

		String pingResp = ClientIO.downloadNovaPage(env, odooPing, null, null);
		String healthResp = ClientIO.downloadNovaPage(env, odooHealth, null, null);

		Boolean pingOK = (pingResp == null) ? null //
				: TextUtil.discardWhitespace(pingResp).contains("{\"result\":true");
		Boolean healthOK = (healthResp == null) ? null //
				: TextUtil.discardWhitespace(healthResp).contains("{\"result\":true");

		return new Boolean[] { pingOK, healthOK };
	}

	public static final Boolean[] checkJBilling(String env) throws Exception {
		String jbillingPing = "/billing-esbe/api/rest/check/ping";
		String jbillingHealth = "/billing-esbe/api/rest/check/health";

		String pingResp = ClientIO.downloadNovaPage(env, jbillingPing, null, null);
		String healthResp = ClientIO.downloadNovaPage(env, jbillingHealth, null, null);

		Boolean pingOK = (pingResp == null) ? null //
				: TextUtil.discardWhitespace(pingResp).contains("<rs-response><result>true</result></rs-response>");
		Boolean healthOK = (healthResp == null) ? null //
				: TextUtil.discardWhitespace(healthResp).contains("<checkResponse><result>true</result><checkResults>");

		return new Boolean[] { pingOK, healthOK };
	}

	public static final Boolean[] checkBPM(String env) throws Exception {
		String bpmPing = "/essent-bpm-be/app/rest/ping";
		String bpmHealth = "/essent-bpm-be/app/rest/health";

		String[] creds = Config.getBPMCredentialsForHealthChecksWhichIsOddButOkay(env);

		String pingResp = ClientIO.downloadNovaPage(env, bpmPing, creds[0], creds[1]);
		String healthResp = ClientIO.downloadNovaPage(env, bpmHealth, creds[0], creds[1]);

		Boolean pingOK = (pingResp == null) ? null //
				: TextUtil.discardWhitespace(pingResp).contains("<rs-response><result>true</result></rs-response>");
		Boolean healthOK = (healthResp == null) ? null //
				: TextUtil.discardWhitespace(healthResp).contains("<checkResponse><result>true</result><msg>OK</msg>");

		return new Boolean[] { pingOK, healthOK };
	}
}
